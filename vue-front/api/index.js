import request from '@/utils/request'

export default {
  // 查询前八条热门课程，前四名名师
  getIndexData() {
    return request({
      url: '/eduservice/indexfront/index',
      method: 'get'
    })
  }
}
