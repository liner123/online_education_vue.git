import reqeust from "@/utils/request"

export default {
  // 讲师页前八名老师
  getTeacherList(page, limit) {
    //?page=1&limit=4
    return reqeust({
      url: `/eduservice/teacherfront/getTeacherList/${page}/${limit}`,
      method: 'post',
    })
  },
  // 查询讲师详情
  getTeacherInfo(teacherId) {
    return reqeust({
      url: `/eduservice/teacherfront/getTeacherInfo/${teacherId}`,
      method: 'get'
    })
  }
}
