import request from '@/utils/request'

export default {
  // 注册
  register(params) {
    return request({
      url: '/ucenter/member/register',
      method: 'post',
      data: params
    })
  },
  // 发送短信
  getMobile(phone) {
    return request({
      url: `/edumsm/operation/send/${phone}`,
      method: 'get'
    })
  }
}
