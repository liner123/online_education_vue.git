import request from '@/utils/request'

export default {
  getLoginInfo() {
    return request({
      url: '/ucenter/member/getUserInfoByToken',
      method: 'get'
    })
  },
  submitLogin(userInfo) {
    return request({
      url: '/ucenter/member/login',
      method: 'post',
      data: userInfo
    })
  }
}
