import request from '@/utils/request'

export default {
  // 条件查询课程
  getCourseListByCondition(page, limit, condition) {
    return request({
      url: `/eduservice/coursefront/getCoursePage/${page}/${limit}`,
      method: 'post',
      data: condition
    })
  },
  // 获取所有学科信息
  getAllSubjectInfo() {
    return request({
      url: `/eduservice/subject/getSubject`,
      method: 'get'
    })
  },
  // 获取课程详情
  getCourseDetailInfo(courseId) {
    return request({
      url: `/eduservice/coursefront/getFrontCourseInfo/${courseId}`,
      method: 'get'
    })
  },
  // 生成二维码
  createNavie(orderNo) {
    return request({
      url: `/eduorder/paylog/createQRCode/${orderNo}`,
      method: 'post'
    })
  },
  // 查询订单支付的状态
  getPayStatus(orderNo) {
    return request({
      url: `/eduorder/paylog/getPayStatus/${orderNo}`,
      method: 'get'
    })
  },
  // 添加评论
  addComment(courseId, context) {
    return request({
      url: `/eduservice/comment/addComment/${courseId}`,
      method: 'post',
      data: context
    })
  },
  // 获取评论
  getComment(current, limit, courseId) {
    return request({
      url: `/eduservice/comment/getComment/${current}/${limit}/${courseId}`,
      method: 'get',
    })
  },
  // 删除评论
  deleteComment(cid) {
    return request({
      url: `/eduservice/comment/deleteComment/${cid}`,
      method: 'delete'
    })
  }
}
