import request from '@/utils/request'

export default {
  // 获取视频播放的auth
  getPlayerAuth(playerId) {
    return request({
      url: `/eduvod/video/getPlayerAuth/${playerId}`,
      method: 'get'
    })
  }
}
