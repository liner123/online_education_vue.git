import request from '@/utils/request'

export default {

  // 根据courseId 生产订单
  createOrder(courseId) {
    return request({
      url: `/eduorder/order/generate/${courseId}`,
      method: 'post'
    })
  },
  // 根据订单id查询订单信息
  getOrderInfo(orderId) {
    return request({
      url: `/eduorder/order/getOrderInfo/${orderId}`,
      method: 'get'
    })
  }
}
