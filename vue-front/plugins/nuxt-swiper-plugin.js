import Vue from 'vue'
import VueAwesomeSwiper from "vue-awesome-swiper/dist/ssr";
import VueQriously from "vue-qriously/dist/vue-qriously"; // 这是用于微信
import ElementUI from 'element-ui' // element-ui
import 'element-ui/lib/theme-chalk/index.css' // element-ui的css

Vue.use(VueAwesomeSwiper);
Vue.use(ElementUI);
Vue.use(VueQriously);

