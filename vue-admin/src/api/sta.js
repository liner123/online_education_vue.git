import request from '@/utils/request'

export default {
  // 生产数据
  createSta(day) {
    return request({
      url: `/staservice/stadaily/registerCount/${day}`,
      method: 'post'
    })
  },
  // 2获取统计数据
  getDataChar(type, begin, end) {
    return request({
      url: `/staservice/stadaily/showData/${type}/${begin}/${end}`,
      method: 'get'
    })
  }
}
