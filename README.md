# online_education_vue

#### 介绍

online_education的前端代码
采用 Vue 2.x 技术

#### 软件架构
软件架构说明

vue-admin 后台项目
vue-front 前台项目

#### 安装教程

1.  git clone https://gitee.com/liner123/new-gulimall-vue.git
2.  使用 vscode 或者 WebStorm 打开
3.  cd vue-admin 或 vue-front
4.  npm install  (安装依赖)
5.  npm run dev  （启动）

#### 使用说明

无 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
